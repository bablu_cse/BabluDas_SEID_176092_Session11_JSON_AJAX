/**
 * Created by Web App Develop-PHP on 8/19/2017.
 */


//document.write("HelloWorld");
var myJSONvariable={
    "property1" : 123,
    "property2" : "abc"

}
/*document.write(myJSONvariable.property2 +"<br>");
document.write(myJSONvariable.property1 +"<br>");

//if we insert the value
document.write(myJSONvariable.property2="mno" +"<br>");
document.write(myJSONvariable.property1=458 +"<br>");*/

//document.write(myJSONvariable.property1 +"<br>");


/*var age =[];
age[0]=123;
age["Rahim"]=30;
age["Karim"]=43;
age["Abul"]=52;
document.write(age.Abul  +"<br>");*/


/*var animals=[

    {
        "name" : "MeowsALot",
        "species" : "Cat",
        "food" : "Tuna"
    },
    {
        "name" : "BarksALot",
        "species" : "Dog",
        "food" : "Meat"
    }
];
document.write(animals[0].food +"<br>");*/

var animals = [

    {
        "name" : "MeowsALot",
        "species" : "Cat",
        "food" : {
            "likes" : ["Tuna","Fish","Meat"],
            "dislikes" : ["DogFood","Grass"]
        }
    },
    {
        "name" : "BarksALot",
        "species" : "Dog",
        "food" : {
            "likes" : ["Meat","Fish","DogFood"],
            "dislikes" : ["CatFood","Grass"]
        }
    },
    {
        "name" : "CharmingALot",
        "species" : "Brid",
        "food" : {
            "likes" : ["Insects","Rice"],
            "dislikes" : ["Meat","Fish"]
        }
    }
];

for (i=0;i<animals.length;i++){

    var myHTML = "Name :" +animals[i].name + "<br>";
    myHTML += "Species :" +animals[i].species + "<br>";
    myHTML += "Food : <br> &nbsp;&nbsp; Likes : " +animals[i].food.likes + "<br>";
    myHTML += " &nbsp;&nbsp; DisLikes : " +animals[i].food.dislikes + "<br>";
    
    document.write(myHTML +  "<hr>");
}